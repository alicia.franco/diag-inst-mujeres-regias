#
# Author: Gina Jiménez
# Maintainer(s): GJ, OE, AL
# License: (c) Data Cívica 2021, GPL v2 or newer
#
# -----------------------------------------------
# diagnostico-instituto-regio/descriptives/src/grafs.R

require(pacman)
p_load(tidyverse, here, janitor, treemapify, googledrive, svglite)

paths <- list(
  desc = here("descriptives/"),
  inp = here("descriptives/inp/"),
  out = here("descriptives/out/")
)

source(paste0(paths$desc, "src/tema.R"))
data <- readRDS(paste0(paths$inp, "sinais-all.rds"))

#### Total mujeres migrantes ####
data %>%
  filter(geogra == "Monterrey") %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  group_by(year, migrante) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  ggplot(aes(x = year, fill = migrante, y = total)) +
  geom_bar(stat = "identity", position = "dodge") +
  geom_text(
    aes(label = total),
    position = position_dodge(width = 1),
    family = "Barlow Condensed",
    vjust = -1,
    size = 5
  ) +
  labs(
    title = "Total de mujeres migrantes y no migrantes asesinadas en Monterrey",
    subtitle = "A partir de 2015",
    caption = cap_mort,
    fill = "",
    y = ""
  ) +
  tema +
  scale_fill_manual(values = pal_base) +
  ylim(0, 50)

devices <- c("jpg", "svg")
walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/total_migrantes."), .x),
       device = .x,
       width = 10,
       height = 6
     ))


#### Total por educación ####
#### Limpiar población ####
pob_muj <- readRDS(paste0(paths$inp, "pob.rds")) %>%
  clean_names() %>%
  mutate(clave = str_pad(clave, 5, "left", "0")) %>%
  rename(inegi = clave) %>%
  filter(sexo == "mujeres") %>%
  select(-sexo,-edad,-municipio) %>%
  pivot_longer(2:32, values_to = "pob", names_to = "year") %>%
  mutate(
    cve_ent = substr(inegi, 0, 2),
    year = gsub("x", "", year),
    year = as.numeric(year),
    geogra = ifelse(
      inegi == "19039",
      "Monterrey",
      ifelse(cve_ent == 19, "Nuevo León", "Resto del país")
    )
  ) %>%
  group_by(year, geogra) %>%
  summarize(pob = sum(pob, na.rm = T)) %>%
  filter(year < 2021)

#### Dónde mueren ####
data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  filter(!is.na(lugar)) %>%
  group_by(lugar, geogra) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  group_by(geogra) %>%
  mutate(den = sum(total, na.rm = T),
         per = total / den * 100) %>%
  ungroup() %>%
  ggplot(aes(
    area = per,
    fill = lugar,
    label = paste(round(per, 1), "%")
  )) +
  geom_treemap() +
  geom_treemap_text(
    place = "centre",
    grow = F,
    color = "white",
    family = 'Rubik'
  )  +
  facet_wrap( ~ geogra) +
  scale_fill_manual(values = pal_base) +
  labs(
    title = "¿Dónde mueren las mujeres en Monterrey, en Nuevo León y en el país?",
    subtitle = "Modo de ocurrencia de los homicidios de mujeres del 2015 al 2020",
    caption = cap_mort,
    fill = ""
  ) +
  tema

walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/lugar_geo."), .x),
       device = .x,
       width = 10,
       height = 6
     ))

#### Cómo mueren ####
data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  filter(lugar == "Vivienda" | lugar == "Vía pública") %>%
  filter(!is.na(causa_hom)) %>%
  group_by(causa_hom, geogra, lugar) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  group_by(geogra, lugar) %>%
  mutate(den = sum(total, na.rm = T),
         per = total / den * 100) %>%
  ungroup() %>%
  ggplot(aes(
    area = per,
    fill = causa_hom,
    subgroup = lugar,
    label = paste(round(per, 1), "%")
  )) +
  geom_treemap() +
  geom_treemap_text(
    place = "centre",
    grow = F,
    color = "white",
    family = 'Rubik'
  )  +
  geom_treemap_subgroup_border(color = "white") +
  geom_treemap_subgroup_text(
    color = "white",
    family = "Rubik",
    place = "bottomright",
    size = "24",
    grow = F
  ) +
  facet_wrap( ~ geogra) +
  labs(
    title = "¿Cómo mueren las mujeres que matan en Monterrey, Nuevo León y en todo México?",
    subtitle = "Según lugar de ocurrencia a partir de 2015",
    caption = cap_mort,
    fill = "",
    y = ""
  ) +
  tema +
  scale_fill_manual(values = pal_base)

walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/modo_lugar_geo."), .x),
       device = .x,
       width = 10,
       height = 6
     ))

#### Tasa por área ####
data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 1999) %>%
  group_by(year, geogra) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  left_join(., pob_muj) %>%
  mutate(tasa = total / pob * 100000) %>%
  ggplot(aes(x = year, y = tasa, color = geogra)) +
  geom_line(size = 2) +
  geom_point(size = 3) +
  tema +
  scale_color_manual(values = pal_base) +
  labs(
    title = "Tasa de mujeres asesinadas en Nuevo León, Monterrey y el resto del país",
    caption = cap_mort,
    color = "",
    y = "",
    x = ""
  )

walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/tasa_mujeres."), .x),
       device = .x,
       width = 10,
       height = 6
     ))

data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 1999) %>%
  filter(lugar == "Vivienda" | lugar == "Vía pública") %>%
  group_by(year, geogra, lugar) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  left_join(., pob_muj) %>%
  mutate(tasa = total / pob * 100000) %>%
  ggplot(aes(x = year, y = tasa, color = geogra)) +
  geom_line(size = 2) +
  geom_point(size = 3) +
  tema +
  scale_color_manual(values = pal_base) +
  labs(
    title = "Tasa de mujeres asesinadas en Nuevo León, Monterrey y el resto del país",
    caption = cap_mort,
    color = "",
    y = "",
    x = ""
  ) +
  facet_wrap( ~ lugar)
walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/tasa_mujeres2."), .x),
       device = .x,
       width = 10,
       height = 6
     ))


#### Dónde mueren por escolaridad ####
data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  filter(escolarida != "No especificado") %>%
  filter(!is.na(escolarida)) %>%
  filter(geogra != "Resto del país") %>%
  group_by(lugar, geogra, escolarida) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  group_by(geogra, escolarida) %>%
  mutate(den = sum(total, na.rm = T),
         per = total / den * 100,
         geogra = factor(geogra, levels = c("Monterrey", "Nuevo León"))) %>%
  ungroup() %>%
  ggplot(aes(
    area = per,
    fill = lugar,
    subgroup = factor(geogra, levels = c("Monterrey", "Nuevo León")),
    label = paste(round(per, 1), "%")
  )) +
  geom_treemap() +
  geom_treemap_subgroup_border(color = "white") +
  geom_treemap_subgroup_text(
    color = "white",
    family = "Rubik",
    place = "bottomright",
    size = 8,
    grow = F
  ) +
  geom_treemap_text(
    place = "centre",
    grow = F,
    color = "white",
    family = 'Rubik'
  )  +
  facet_wrap( ~ escolarida) +
  scale_fill_manual(values = pal_base) +
  labs(
    title = "¿Dónde mueren las mujeres en Monterrey y en Nuevo León?",
    subtitle = "Modo de ocurrencia de los homicidios de mujeres del 2015 al 2020",
    caption = cap_mort,
    fill = ""
  ) +
  tema

walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/lugar_geo_es."), .x),
       device = .x,
       width = 10,
       height = 6
     ))

data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  filter(escolarida != "No especificado") %>%
  filter(!is.na(escolarida)) %>%
  filter(geogra != "Resto del país") %>%
  group_by(lugar, geogra, migrante) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  group_by(geogra, migrante) %>%
  mutate(den = sum(total, na.rm = T),
         per = total / den * 100) %>%
  ungroup() %>%
  ggplot(aes(
    area = per,
    fill = lugar,
    subgroup = geogra,
    label = paste(round(per, 1), "%")
  )) +
  geom_treemap() +
  geom_treemap_subgroup_border(color = "white") +
  geom_treemap_subgroup_text(
    color = "white",
    family = "Rubik",
    place = "bottomright",
    size = "24",
    grow = F
  ) +
  geom_treemap_text(
    place = "centre",
    grow = F,
    color = "white",
    family = 'Rubik'
  )  +
  facet_wrap( ~ migrante) +
  scale_fill_manual(values = pal_base) +
  labs(
    title = "¿Dónde mueren las mujeres en Monterrey, en Nuevo León y en el país?",
    subtitle = "Lugar de ocurrencia de los homicidios de mujeres del 2015 al 2020",
    caption = cap_mort,
    fill = ""
  ) +
  tema
walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/lugar_geo_mig."), .x),
       device = .x,
       width = 10,
       height = 6
     ))

#### Cómo mueren por escolaridad y condición migrante####
data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  filter(!is.na(causa_hom)) %>%
  filter(!is.na(escolarida)) %>%
  group_by(causa_hom, geogra, escolarida) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  group_by(geogra, escolarida) %>%
  mutate(den = sum(total, na.rm = T),
         per = total / den * 100) %>%
  ungroup() %>%
  ggplot(aes(x = escolarida, y = per, fill = causa_hom)) +
  geom_bar(stat = "identity", position = "stack") +
  facet_wrap( ~ geogra) +
  labs(
    title = "¿Cómo mueren las mujeres que matan en Monterrey,\nNuevo León y en todo México?",
    subtitle = "Según su escolaridad a partir de 2015",
    caption = cap_mort,
    fill = "",
    y = "",
    x = ""
  ) +
  tema +
  scale_fill_manual(values = pal_base) +
  coord_flip()
walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/modo_lugar_geo_esc."), .x),
       device = .x,
       width = 10,
       height = 6
     ))

data %>%
  filter(sexo == "Mujer") %>%
  filter(year > 2014) %>%
  filter(!is.na(causa_hom)) %>%
  group_by(causa_hom, geogra, migrante) %>%
  summarize(total = n()) %>%
  ungroup() %>%
  group_by(geogra, migrante) %>%
  mutate(den = sum(total, na.rm = T),
         per = total / den * 100) %>%
  ungroup() %>%
  ggplot(aes(
    area = per,
    fill = causa_hom,
    subgroup = migrante,
    label = paste(round(per, 1), "%")
  )) +
  geom_treemap() +
  geom_treemap_text(
    place = "centre",
    grow = F,
    color = "white",
    family = 'Rubik'
  )  +
  geom_treemap_subgroup_border(color = "white") +
  geom_treemap_subgroup_text(
    color = "white",
    family = "Rubik",
    place = "bottomright",
    size = "24",
    grow = F
  ) +
  facet_wrap( ~ geogra) +
  labs(
    title = "¿Cómo mueren las mujeres que matan en Monterrey, Nuevo León y en todo México?",
    subtitle = "Según si son migrantes, a partir de 2015",
    caption = cap_mort,
    fill = "",
    y = ""
  ) +
  tema +
  scale_fill_manual(values = pal_base)

walk(devices,
     ~ ggsave(
       filename = paste0(here("descriptives/out/modo_lugar_geo_mig."), .x),
       device = .x,
       width = 10,
       height = 6
     ))
