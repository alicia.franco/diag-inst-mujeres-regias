#
# Author: Alicia Franco
# Maintainer(s): OE, AF, GJ
# License: (c) Data Cívica 2020, GPL v2 or newer
# --------------------------------------------------
# diagnostico-instituto-regio/descriptives/src/tema.R
#

if(!require(pacman))install.packages("pacman")
pacman::p_load(tidyverse, ggrepel, scales, patchwork, extrafont,
               tidyquant, ggnewscale,  magick, here,
               add2ggplot)

extrafont::loadfonts(quiet=T)

Sys.setlocale("LC_ALL", "es_ES.UTF-8") 
options(scipen = 9999)

tema <-  theme_minimal() +
  theme(text = element_text(family = "Rubik", color = "grey35"),
        plot.title = element_text(size = 20, face = "bold", color = "black", hjust = 0.5),
        plot.subtitle = element_text(size = 16, face = "bold", color = "#666666", hjust = 0.5),
        plot.caption = element_text(hjust = 0, size = 10, face = "italic"),
        panel.grid = element_line(linetype = 2), 
        legend.position = "top",
        legend.title = element_text(size = 14, face = "bold"),
        legend.text = element_text(size = 12),
        axis.title = element_text(size = 14, face = "bold"),
        axis.text = element_text(size = 12, face = "bold"),
        strip.background = element_rect(fill="#525252"),
        strip.text = element_text(size=12, face = "bold", color = "#FAF9F6")
  )

pal_base <-  c("#6B6D75", "#F3C1B9", "#384D57", "#CC8D4E", 
               "#393B42", "#A07873", "#5E8C9E", "#A56F3D") 

devices <- c("jpg", "svg")

cap_endi <- "Fuente: Elaboración propia con datos de la ENDIREH (2016)"

cap_mort <- "Fuente: Elaboración propia con datos de mortalidad del INEGI" 


# done.
